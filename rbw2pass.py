#!/usr/bin/env python3

import json
import os
import subprocess
from dataclasses import dataclass
from typing import Any, Iterable, TypedDict

RBW = os.environ.get("RBW", "rbw")
PASS = os.environ.get("PASS", "pass")
PREFIX = os.environ.get("PASS_BW_PREFIX", "bitwarden/")


def rbw_sync():
    subprocess.run([RBW, "sync"], check=True)


def rbw_unlock():
    subprocess.run([RBW, "unlock"], check=True)


def rbw_lock():
    subprocess.run([RBW, "lock"], check=True)


def get_ids() -> Iterable[str]:
    proc = subprocess.run([RBW, "ls", "--fields", "id"],
                          check=True, capture_output=True)
    ids = map(bytes.decode, proc.stdout.splitlines())
    return ids


class RawPassURIs(TypedDict):
    uri: str
    match_type: int


class RawPassData(TypedDict):
    username: str | None
    password: str
    totp: str | None
    uris: list[RawPassURIs]


class RawPassword(TypedDict):
    id: str
    folder: str | None
    name: str
    data: RawPassData | None
    fields: list[str]
    notes: str | None
    history: list[Any]  # not sure yet


def get_passwords() -> Iterable[RawPassword]:
    for pass_id in get_ids():
        proc = subprocess.run([RBW, "get", "--raw", pass_id],
                              check=True, capture_output=True)
        yield json.loads(proc.stdout)


@dataclass
class PassEntry:
    name: str
    password: str

    def create(self, prefix: str = ""):
        if prefix:
            key_name = f"{prefix}{self.name}"
        else:
            key_name = self.name

        proc = subprocess.run([PASS, key_name],
                              capture_output=True)

        if proc.returncode == 0 and proc.stdout.decode() == self.password:
            return

        proc = subprocess.run([PASS, "insert", "-m", "-f", key_name],
                              check=True,
                              input=self.password.encode())
        assert proc.returncode == 0


def gen_pass_entry(raw_pass: RawPassword) -> PassEntry:
    if raw_pass["data"] and raw_pass["data"]["password"]:
        password = raw_pass["data"]["password"]
    else:
        password = raw_pass["notes"]

    assert password is not None

    if raw_pass["data"] and raw_pass["data"]["username"]:
        name = f"{raw_pass['name']}/{raw_pass['data']['username']}"
    else:
        name = raw_pass["name"]

    return PassEntry(name, password + "\n")


def main(prefix: str = ""):
    rbw_sync()
    rbw_unlock()
    for entry in map(gen_pass_entry, get_passwords()):
        entry.create(prefix)
    rbw_lock()


if __name__ == "__main__":
    main(PREFIX)
